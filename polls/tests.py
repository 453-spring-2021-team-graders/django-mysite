from django.test import TestCase

# Create your tests here.
from django.utils.timezone import now
from datetime import timedelta
from .models import Question, Choice
from django.urls import reverse
from django.test import Client
from parameterized import parameterized


class QuestionModelTest(TestCase):
    def test_question_text_field_value(self):
        question = Question.objects.create(question_text='test question',
                                           pub_date=now())
        self.assertEqual('test question', question.question_text)

    @parameterized.expand([
        (now() + timedelta(days=30), False),
        (now() - timedelta(days=1, seconds=1), False),
        (now() - timedelta(hours=23, minutes=59, seconds=59), True),
    ])
    def test_was_published_recently(self, input, expected):
        question = Question(pub_date=input)
        self.assertEqual(question.was_published_recently(), expected)


class ChoiceModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        question = Question.objects.create(question_text='test question',
                                           pub_date=now())
        Choice.objects.create(question=question, choice_text="choice 1")
        Choice.objects.create(question=question, choice_text="choice 2",
                              votes=1)

    def test_choice_test_field_value(self):
        choice = Choice.objects.get(id=1)
        self.assertEqual('choice 1', choice.choice_text)

    def test_choice_votes_default_value(self):
        choice1 = Choice.objects.get(id=1)
        choice2 = Choice.objects.get(id=2)
        self.assertEqual(0, choice1.votes)
        self.assertEqual(1, choice2.votes)


class QuestionIndexViewTests(TestCase):
    @staticmethod
    def create_question(question_text, days):
        time = now() + timedelta(days=days)
        return Question.objects.create(question_text=question_text,
                                       pub_date=time)

    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    @parameterized.expand([
        ("Past question.", -30, ['<Question: Past question.>']),
        ("Future question.", 10, []),
    ])
    def test_single_question(self, inp1, inp2, expected):
        QuestionIndexViewTests.create_question(question_text=inp1, days=inp2)
        response = self.client.get(reverse('polls:index'))
        context_obj = response.context['latest_question_list']
        self.assertQuerysetEqual(context_obj, expected)

    @parameterized.expand([
        (
            "Past Question 1.", -30, "Past Question 2.", -5,
            ['<Question: Past Question 2.>', '<Question: Past Question 1.>']
        ),
        (
            "Future Question 1.", 30, "Past Question 1.", -5,
            ['<Question: Past Question 1.>']
        ),
        (
            "Future Question 1.", 30, "Future Question 2.", 500,
            []
        ),
    ])
    def test_two_questions(self, inp1, inp2, inp3, inp4, expected):
        """
        The questions index page display multiple questions.
        """
        QuestionIndexViewTests.create_question(question_text=inp1, days=inp2)
        QuestionIndexViewTests.create_question(question_text=inp3, days=inp4)
        response = self.client.get(reverse('polls:index'))
        context_obj = response.context['latest_question_list']
        self.assertQuerysetEqual(context_obj, expected)
